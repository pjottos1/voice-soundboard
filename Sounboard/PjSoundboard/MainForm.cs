﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PjSoundboard
{
    public partial class MainForm : Form
    {
        IList<Sound> sounds;
        IList<ListViewItem> listViewItems;

        Mp3BytesPlayer player;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            sounds = new List<Sound>();
            listViewItems = new List<ListViewItem>();

            player = new Mp3BytesPlayer();
        }

        private void volumeTrackBar_Scroll(object sender, EventArgs e)
        {
            volumeTextBox.Text = volumeTrackBar.Value.ToString();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = saveOpenDialog.ShowDialog();
            switch(result)
            {
                case DialogResult.OK:
                    DisposeSaveFileReader reader = new DisposeSaveFileReader(saveOpenDialog.FileName);
                    sounds = reader.ReadSoundsFromSave();

                    foreach(Sound sound in sounds)
                    {
                        ListViewItem viewItem = new ListViewItem(new string[2] { sound.Name, stringFromArray(sound.Commands) });
                        soundListView.Items.Add(viewItem);
                    }
                    break;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = saveBoardDialog.ShowDialog();
            switch(result)
            {
                case DialogResult.OK:
                    IList<byte> bytesToAdd = new List<byte>();

                    foreach(Sound sound in sounds)
                    {
                        foreach(byte soundByte in sound.ToByteArray())
                        {
                            bytesToAdd.Add(soundByte);
                        }
                    }

                    File.WriteAllBytes(saveBoardDialog.FileName, bytesToAdd.ToArray());
                    break;
            }
        }

        private void volumeTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                volumeTrackBar.Value = int.Parse(volumeTextBox.Text);
            }
            catch(FormatException)
            {
                volumeTrackBar.Value = 0;
            }
        }

        private void addSoundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            soundOpenDialog.ShowDialog();

            for(int i = 0; i < soundOpenDialog.FileNames.Length; i++)
            {
                AddSoundDialog newSoundDialog = new AddSoundDialog(soundOpenDialog.FileNames[i], soundOpenDialog.SafeFileNames[i]);
                newSoundDialog.ShowDialog();

                string fullpath = Path.GetFullPath(newSoundDialog.SoundPath);
                sounds.Add(new Sound(fullpath, newSoundDialog.SoundName, newSoundDialog.SoundCommands));
                soundListView.Items.Add(new ListViewItem(new string[2] { newSoundDialog.SoundName, stringFromArray(newSoundDialog.SoundCommands) }));
            }
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            int index = soundListView.SelectedIndices[0];
            player.PlayBytes(sounds[index].SoundBytes);
        }

        private string stringFromArray(string[] array)
        {
            for(int i = 1; i < array.Length; i++)
            {
                if (i == array.Length - 1) array[0] += array[i];
                else array[0] += array[i] + ", ";
            }
            return array[0];
        }
    }
}
