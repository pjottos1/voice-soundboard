﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PjSoundboard
{
    public partial class AddSoundDialog : Form
    {
        public string SoundPath;
        public string SoundName;
        public string[] SoundCommands;

        public AddSoundDialog(string fullPath = "", string fileName = "", string[] commands = null)
        {
            SoundPath = fullPath;
            SoundName = fileName;
            SoundCommands = new string[2] { "hi mark", "memes x d" };
            //TODO load the existing commands into the dialog
            InitializeComponent();
        }

        private void AddSoundDialog_Load(object sender, EventArgs e)
        {
            pathTextBox.Text = SoundPath;
            soundNameTextBox.Text = SoundName;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            SoundPath = Path.GetFullPath(pathTextBox.Text);
            SoundName = soundNameTextBox.Text;
        }
    }
}
