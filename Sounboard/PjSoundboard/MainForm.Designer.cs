﻿namespace PjSoundboard
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.volumeTrackBar = new System.Windows.Forms.TrackBar();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.soundListView = new System.Windows.Forms.ListView();
            this.soundName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.soundCommands = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rightclickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addSoundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editSoundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeSoundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.volumeTextBox = new System.Windows.Forms.TextBox();
            this.deafButton = new System.Windows.Forms.Button();
            this.listenButton = new System.Windows.Forms.Button();
            this.stopSoundsButton = new System.Windows.Forms.Button();
            this.soundOpenDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveBoardDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveOpenDialog = new System.Windows.Forms.OpenFileDialog();
            this.playButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).BeginInit();
            this.rightclickMenu.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // volumeTrackBar
            // 
            this.volumeTrackBar.Location = new System.Drawing.Point(501, 80);
            this.volumeTrackBar.Maximum = 100;
            this.volumeTrackBar.Name = "volumeTrackBar";
            this.volumeTrackBar.Size = new System.Drawing.Size(183, 45);
            this.volumeTrackBar.TabIndex = 0;
            this.volumeTrackBar.Scroll += new System.EventHandler(this.volumeTrackBar_Scroll);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(12, 30);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(183, 20);
            this.searchTextBox.TabIndex = 1;
            this.searchTextBox.Text = "Search sound";
            // 
            // soundListView
            // 
            this.soundListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.soundName,
            this.soundCommands});
            this.soundListView.ContextMenuStrip = this.rightclickMenu;
            this.soundListView.Location = new System.Drawing.Point(12, 56);
            this.soundListView.Name = "soundListView";
            this.soundListView.Size = new System.Drawing.Size(483, 382);
            this.soundListView.TabIndex = 2;
            this.soundListView.UseCompatibleStateImageBehavior = false;
            this.soundListView.View = System.Windows.Forms.View.Details;
            // 
            // soundName
            // 
            this.soundName.Text = "Sound name";
            this.soundName.Width = 195;
            // 
            // soundCommands
            // 
            this.soundCommands.Text = "Commands";
            this.soundCommands.Width = 284;
            // 
            // rightclickMenu
            // 
            this.rightclickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSoundToolStripMenuItem,
            this.editSoundToolStripMenuItem,
            this.removeSoundToolStripMenuItem});
            this.rightclickMenu.Name = "rightclickMenu";
            this.rightclickMenu.Size = new System.Drawing.Size(154, 70);
            // 
            // addSoundToolStripMenuItem
            // 
            this.addSoundToolStripMenuItem.Name = "addSoundToolStripMenuItem";
            this.addSoundToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.addSoundToolStripMenuItem.Text = "Add sound";
            this.addSoundToolStripMenuItem.Click += new System.EventHandler(this.addSoundToolStripMenuItem_Click);
            // 
            // editSoundToolStripMenuItem
            // 
            this.editSoundToolStripMenuItem.Name = "editSoundToolStripMenuItem";
            this.editSoundToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.editSoundToolStripMenuItem.Text = "Edit sound";
            // 
            // removeSoundToolStripMenuItem
            // 
            this.removeSoundToolStripMenuItem.Name = "removeSoundToolStripMenuItem";
            this.removeSoundToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.removeSoundToolStripMenuItem.Text = "Remove sound";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(696, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.newToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(501, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Volume";
            // 
            // volumeTextBox
            // 
            this.volumeTextBox.Location = new System.Drawing.Point(549, 61);
            this.volumeTextBox.Name = "volumeTextBox";
            this.volumeTextBox.Size = new System.Drawing.Size(25, 20);
            this.volumeTextBox.TabIndex = 5;
            this.volumeTextBox.Text = "0";
            this.volumeTextBox.TextChanged += new System.EventHandler(this.volumeTextBox_TextChanged);
            // 
            // deafButton
            // 
            this.deafButton.Location = new System.Drawing.Point(600, 348);
            this.deafButton.Name = "deafButton";
            this.deafButton.Size = new System.Drawing.Size(90, 90);
            this.deafButton.TabIndex = 6;
            this.deafButton.Text = "Stop listening";
            this.deafButton.UseVisualStyleBackColor = true;
            // 
            // listenButton
            // 
            this.listenButton.Location = new System.Drawing.Point(504, 348);
            this.listenButton.Name = "listenButton";
            this.listenButton.Size = new System.Drawing.Size(90, 90);
            this.listenButton.TabIndex = 7;
            this.listenButton.Text = "Listen for commands";
            this.listenButton.UseVisualStyleBackColor = true;
            // 
            // stopSoundsButton
            // 
            this.stopSoundsButton.Location = new System.Drawing.Point(600, 267);
            this.stopSoundsButton.Name = "stopSoundsButton";
            this.stopSoundsButton.Size = new System.Drawing.Size(75, 75);
            this.stopSoundsButton.TabIndex = 8;
            this.stopSoundsButton.Text = "Stop sounds";
            this.stopSoundsButton.UseVisualStyleBackColor = true;
            // 
            // soundOpenDialog
            // 
            this.soundOpenDialog.Filter = "MP3 format audio files|*.mp3";
            // 
            // saveBoardDialog
            // 
            this.saveBoardDialog.Filter = "PJ Soundboard save files|*.pjs";
            // 
            // playButton
            // 
            this.playButton.Location = new System.Drawing.Point(519, 267);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(75, 75);
            this.playButton.TabIndex = 9;
            this.playButton.Text = "Play manually";
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 450);
            this.Controls.Add(this.playButton);
            this.Controls.Add(this.stopSoundsButton);
            this.Controls.Add(this.listenButton);
            this.Controls.Add(this.deafButton);
            this.Controls.Add(this.volumeTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.soundListView);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.volumeTrackBar);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "PJ Soundboard";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).EndInit();
            this.rightclickMenu.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar volumeTrackBar;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.ListView soundListView;
        private System.Windows.Forms.ColumnHeader soundName;
        private System.Windows.Forms.ColumnHeader soundCommands;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox volumeTextBox;
        private System.Windows.Forms.Button deafButton;
        private System.Windows.Forms.Button listenButton;
        private System.Windows.Forms.Button stopSoundsButton;
        private System.Windows.Forms.ContextMenuStrip rightclickMenu;
        private System.Windows.Forms.ToolStripMenuItem addSoundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editSoundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeSoundToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog soundOpenDialog;
        private System.Windows.Forms.SaveFileDialog saveBoardDialog;
        private System.Windows.Forms.OpenFileDialog saveOpenDialog;
        private System.Windows.Forms.Button playButton;
    }
}

