﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System.IO;

namespace PjSoundboard
{
    class Mp3BytesPlayer
    {
        MixingSampleProvider mixer;
        IWavePlayer outputDevice;

        public Mp3BytesPlayer(int sampleRate = 44100, int channelCount = 2)
        {
            mixer = new MixingSampleProvider(Mp3WaveFormat.CreateIeeeFloatWaveFormat(sampleRate, channelCount));
            mixer.ReadFully = true;
            outputDevice = new WaveOutEvent();
            outputDevice.Init(mixer);
            outputDevice.Play();
        }

        public void PlayBytes(byte[] toPlay)
        {
            IWaveProvider waveProvider = new RawSourceWaveStream(
                new MemoryStream(toPlay),
                mixer.WaveFormat);
            mixer.AddMixerInput(waveProvider);
        }

    }
}
