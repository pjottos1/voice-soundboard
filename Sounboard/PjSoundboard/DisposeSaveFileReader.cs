﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Security.Permissions;
using System.Security;
using System.Threading;
using Microsoft.Win32.SafeHandles;
using System.Runtime.ConstrainedExecution;

namespace PjSoundboard
{
    [SuppressUnmanagedCodeSecurity()]
    internal static class NativeMethods
    {
        //win32 constants
        internal const int GENERIC_READ = unchecked((int)0x80000000);

        //allocate file in kernel and return a handle
        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        internal extern static IntPtr CreateFile(
           String fileName,
           int dwDesiredAccess, 
           System.IO.FileShare dwShareMode,
           IntPtr securityAttrs_MustBeZero, 
           System.IO.FileMode dwCreationDisposition,
           int dwFlagsAndAttributes, 
           IntPtr hTemplateFile_MustBeZero);

        //release file and handle from kernel
        [DllImport("kernel32", SetLastError = true)]
        internal extern static bool CloseHandle(IntPtr handle);
    }

    class DisposeSaveFileReader : IDisposable
    {
        private SafeFileHandle _handle;
        private FileStream fileStream;

        #region id constants
        static readonly byte[] newSoundID = new byte[4] { 0x13, 0x37, 0x69, 0x42 };
        static readonly byte[] newPartID = new byte[4] { 0xFF, 0xFF, 0xFF, 0xFF };
        static readonly byte[] newStringID = new byte[4] { 0xFA, 0xFA, 0xFA, 0xFA };
        #endregion

        public DisposeSaveFileReader(string saveFilePath)
        {
            string fullPath = Path.GetFullPath(saveFilePath);
            //this throws an exception if we don't get permission
            new FileIOPermission(FileIOPermissionAccess.Read, fullPath).Demand();

            //create the handle for the file
            _handle = new SafeFileHandle(NativeMethods.CreateFile(fullPath,
                NativeMethods.GENERIC_READ,
                FileShare.Read,
                IntPtr.Zero,
                FileMode.Open,
                0,
                IntPtr.Zero), true);

            //get a stream for the handle
            fileStream = new FileStream(_handle, FileAccess.Read);
        }

        public byte[] ReadAll()
        {
            byte[] bytes = new byte[fileStream.Length];
            fileStream.ReadAsync(bytes, 0, (int)fileStream.Length);
            fileStream.Position = 0;
            return bytes;
        }

        public IList<Sound> ReadSoundsFromSave()
        {
            IList<Sound> sounds = new List<Sound>();
            IList<byte> lastBytes = new List<byte>();

            fileStream.Seek(0, SeekOrigin.Begin);

            for (int k = 0; k < fileStream.Length; k++)
            {
                byte curByte = (byte)fileStream.ReadByte();

                if(curByte == newSoundID[0])
                {
                    byte[] buffer = new byte[4];
                    buffer[0] = curByte;
                    int bytesRead = fileStream.ReadAsync(buffer, 1, buffer.Length - 1).Re;
                    if(buffer == newSoundID)
                    {
                        sounds.Add(new Sound(lastBytes.ToArray()));
                        lastBytes = new List<byte>();
                        continue;
                    }
                    else
                    {
                        fileStream.Position -= 3;
                    }
                }
                lastBytes.Add((byte)curByte);
            }
            return sounds;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(_handle != null && !_handle.IsInvalid)
            {
                _handle.Dispose();
                fileStream.Dispose();
            }
        }
    }
}
