﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;

namespace PjSoundboard
{
    class Sound
    {
        public byte[] SoundBytes;
        public string Name;
        public string[] Commands;

        public Sound(string filePath, string name, string[] commands)
        {
            AudioFileReader reader = new AudioFileReader(Path.GetFullPath(filePath));
            SoundBytes = new byte[reader.Length];
            reader.Read(SoundBytes, 0, (int)reader.Length);
            reader.Dispose();

            Name = name;
            Commands = commands;
        }

        public Sound(byte[] byteRepresentation)
        {
            IList<byte>[] addTo = new IList<byte>[3];
            addTo[0] = new List<byte>(); //sound bytes
            addTo[1] = new List<byte>(); //name bytes
            addTo[2] = new List<byte>(); //command bytes

            IList<string> commands = new List<string>();

            int writeTo = 0;

            for(int j = 0; j < byteRepresentation.Length; j++)
            {
                //TODO remove that excess comparison
                if (byteRepresentation[j] == 0xFF)
                {
                    for (int i = j; i <= j + 3; i++)
                    {
                        if (byteRepresentation[i] != 0xFF)
                        {
                            break;
                        }
                        //this means if weve reached the end of the identifier, code would not reach here if it was not a complete identifier
                        if (i == j + 3)
                        {
                            writeTo++;
                            j += 4;
                        }
                    }
                }
                else if (byteRepresentation[j] == 0xFA)
                {
                    for (int i = j; i <= j + 3; i++)
                    {
                        if (byteRepresentation[i] != 0xFA)
                        {
                            break;
                        }
                        //this means if weve reached the end of the identifier, code would not reach here if it was not a complete identifier
                        if (i == j + 3)
                        {
                            string command = Encoding.Unicode.GetString(addTo[2].ToArray());
                            commands.Add(command);
                            addTo[2] = new List<byte>();
                            j += 4;
                        }
                    }
                }

                //find the byte list to add to and then add the current byte; already accounted for the identifier size
                addTo[writeTo].Add(byteRepresentation[j]);
            }

            SoundBytes = addTo[0].ToArray();
            Name = Encoding.Unicode.GetString(addTo[1].ToArray());
            Commands = commands.ToArray();
        }

        public byte[] ToByteArray()
        {
            IList<byte> toReturn = new List<byte>(new byte[4] { 0x13, 0x37, 0x69, 0x42 });

            foreach(byte soundByte in SoundBytes)
            {
                toReturn.Add(soundByte);
            }

            toReturn = addElementIdentifier(toReturn);

            foreach(byte nameByte in Encoding.Unicode.GetBytes(Name))
            {
                toReturn.Add(nameByte);
            }

            toReturn = addElementIdentifier(toReturn);

            foreach (string command in Commands)
            {
                foreach(byte commandByte in Encoding.Unicode.GetBytes(command))
                {
                    toReturn.Add(commandByte);
                }
                toReturn = addArrayElementIdentifier(toReturn);
            }

            return toReturn.ToArray();
        }

        private IList<byte> addElementIdentifier(IList<byte> addTo)
        {
            for(int i = 0; i < 4; i++)
            {
                addTo.Add(0xFF);
            }
            return addTo;
        }

        private IList<byte> addArrayElementIdentifier(IList<byte> addTo)
        {
            for (int i = 0; i < 4; i++)
            {
                addTo.Add(0xFA);
            }
            return addTo;
        }
    }
}
