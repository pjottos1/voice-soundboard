﻿namespace Sounboard
{
    partial class AddSoundDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.commandsLabel = new System.Windows.Forms.Label();
            this.pathLabel = new System.Windows.Forms.Label();
            this.soundNameLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelAllButton = new System.Windows.Forms.Button();
            this.commandsListView = new System.Windows.Forms.ListView();
            this.command = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // pathTextBox
            // 
            this.pathTextBox.Location = new System.Drawing.Point(81, 12);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(305, 20);
            this.pathTextBox.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(191, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(195, 20);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(191, 64);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(195, 20);
            this.textBox2.TabIndex = 2;
            // 
            // commandsLabel
            // 
            this.commandsLabel.AutoSize = true;
            this.commandsLabel.Location = new System.Drawing.Point(12, 67);
            this.commandsLabel.Name = "commandsLabel";
            this.commandsLabel.Size = new System.Drawing.Size(173, 13);
            this.commandsLabel.TabIndex = 4;
            this.commandsLabel.Text = "Voice commands to play this sound";
            // 
            // pathLabel
            // 
            this.pathLabel.AutoSize = true;
            this.pathLabel.Location = new System.Drawing.Point(12, 15);
            this.pathLabel.Name = "pathLabel";
            this.pathLabel.Size = new System.Drawing.Size(62, 13);
            this.pathLabel.TabIndex = 5;
            this.pathLabel.Text = "Sound path";
            // 
            // soundNameLabel
            // 
            this.soundNameLabel.AutoSize = true;
            this.soundNameLabel.Location = new System.Drawing.Point(12, 41);
            this.soundNameLabel.Name = "soundNameLabel";
            this.soundNameLabel.Size = new System.Drawing.Size(140, 13);
            this.soundNameLabel.TabIndex = 6;
            this.soundNameLabel.Text = "Name to show for the sound";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(162, 188);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(81, 188);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 8;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelAllButton
            // 
            this.cancelAllButton.Location = new System.Drawing.Point(243, 188);
            this.cancelAllButton.Name = "cancelAllButton";
            this.cancelAllButton.Size = new System.Drawing.Size(75, 23);
            this.cancelAllButton.TabIndex = 9;
            this.cancelAllButton.Text = "Cancel all";
            this.cancelAllButton.UseVisualStyleBackColor = true;
            // 
            // commandsListView
            // 
            this.commandsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.command});
            this.commandsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.commandsListView.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.commandsListView.LabelWrap = false;
            this.commandsListView.Location = new System.Drawing.Point(191, 90);
            this.commandsListView.MaximumSize = new System.Drawing.Size(195, 92);
            this.commandsListView.MinimumSize = new System.Drawing.Size(195, 92);
            this.commandsListView.MultiSelect = false;
            this.commandsListView.Name = "commandsListView";
            this.commandsListView.Size = new System.Drawing.Size(195, 92);
            this.commandsListView.TabIndex = 10;
            this.commandsListView.TileSize = new System.Drawing.Size(60, 7);
            this.commandsListView.UseCompatibleStateImageBehavior = false;
            this.commandsListView.View = System.Windows.Forms.View.List;
            // 
            // command
            // 
            this.command.Text = "Command";
            // 
            // AddSoundDialog
            // 
            this.AcceptButton = this.cancelButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.okButton;
            this.ClientSize = new System.Drawing.Size(398, 223);
            this.ControlBox = false;
            this.Controls.Add(this.commandsListView);
            this.Controls.Add(this.cancelAllButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.soundNameLabel);
            this.Controls.Add(this.pathLabel);
            this.Controls.Add(this.commandsLabel);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pathTextBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddSoundDialog";
            this.Text = "Add a new sound to the board";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label commandsLabel;
        private System.Windows.Forms.Label pathLabel;
        private System.Windows.Forms.Label soundNameLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelAllButton;
        private System.Windows.Forms.ListView commandsListView;
        private System.Windows.Forms.ColumnHeader command;
    }
}