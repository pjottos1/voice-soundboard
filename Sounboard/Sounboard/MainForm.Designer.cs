﻿namespace Sounboard
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSoundboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSoundboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSoundboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.soundListView = new System.Windows.Forms.ListView();
            this.addSoundButton = new System.Windows.Forms.Button();
            this.removeSoundButton = new System.Windows.Forms.Button();
            this.editSoundButton = new System.Windows.Forms.Button();
            this.openSoundDialog = new System.Windows.Forms.OpenFileDialog();
            this.openBoardDialog = new System.Windows.Forms.OpenFileDialog();
            this.volumeTrackBar = new System.Windows.Forms.TrackBar();
            this.volumeLabel = new System.Windows.Forms.Label();
            this.volumeTextBox = new System.Windows.Forms.TextBox();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.gainCheckBox = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(754, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openSoundboardToolStripMenuItem,
            this.saveSoundboardToolStripMenuItem,
            this.newSoundboardToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openSoundboardToolStripMenuItem
            // 
            this.openSoundboardToolStripMenuItem.Name = "openSoundboardToolStripMenuItem";
            this.openSoundboardToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.openSoundboardToolStripMenuItem.Text = "Open soundboard";
            // 
            // saveSoundboardToolStripMenuItem
            // 
            this.saveSoundboardToolStripMenuItem.Name = "saveSoundboardToolStripMenuItem";
            this.saveSoundboardToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.saveSoundboardToolStripMenuItem.Text = "Save soundboard";
            // 
            // newSoundboardToolStripMenuItem
            // 
            this.newSoundboardToolStripMenuItem.Name = "newSoundboardToolStripMenuItem";
            this.newSoundboardToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.newSoundboardToolStripMenuItem.Text = "New soundboard";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // soundListView
            // 
            this.soundListView.Location = new System.Drawing.Point(12, 27);
            this.soundListView.Name = "soundListView";
            this.soundListView.Size = new System.Drawing.Size(450, 298);
            this.soundListView.TabIndex = 1;
            this.soundListView.UseCompatibleStateImageBehavior = false;
            // 
            // addSoundButton
            // 
            this.addSoundButton.Location = new System.Drawing.Point(12, 331);
            this.addSoundButton.Name = "addSoundButton";
            this.addSoundButton.Size = new System.Drawing.Size(75, 32);
            this.addSoundButton.TabIndex = 2;
            this.addSoundButton.Text = "Add Sound";
            this.addSoundButton.UseVisualStyleBackColor = true;
            this.addSoundButton.Click += new System.EventHandler(this.addSoundButton_Click);
            // 
            // removeSoundButton
            // 
            this.removeSoundButton.Location = new System.Drawing.Point(12, 369);
            this.removeSoundButton.Name = "removeSoundButton";
            this.removeSoundButton.Size = new System.Drawing.Size(75, 32);
            this.removeSoundButton.TabIndex = 3;
            this.removeSoundButton.Text = "Remove";
            this.removeSoundButton.UseVisualStyleBackColor = true;
            this.removeSoundButton.Click += new System.EventHandler(this.removeSoundButton_Click);
            // 
            // editSoundButton
            // 
            this.editSoundButton.Location = new System.Drawing.Point(12, 407);
            this.editSoundButton.Name = "editSoundButton";
            this.editSoundButton.Size = new System.Drawing.Size(75, 32);
            this.editSoundButton.TabIndex = 4;
            this.editSoundButton.Text = "Edit";
            this.editSoundButton.UseVisualStyleBackColor = true;
            this.editSoundButton.Click += new System.EventHandler(this.editSoundButton_Click);
            // 
            // openSoundDialog
            // 
            this.openSoundDialog.DefaultExt = "mp3";
            this.openSoundDialog.Filter = "Wave Sound files|*.wav|MP3 sound files|*.mp3";
            this.openSoundDialog.Multiselect = true;
            // 
            // openBoardDialog
            // 
            this.openBoardDialog.Filter = "Soundboard files|*.pj";
            this.openBoardDialog.Multiselect = true;
            // 
            // volumeTrackBar
            // 
            this.volumeTrackBar.Location = new System.Drawing.Point(464, 86);
            this.volumeTrackBar.Maximum = 100;
            this.volumeTrackBar.Name = "volumeTrackBar";
            this.volumeTrackBar.Size = new System.Drawing.Size(278, 45);
            this.volumeTrackBar.TabIndex = 5;
            // 
            // volumeLabel
            // 
            this.volumeLabel.AutoSize = true;
            this.volumeLabel.Location = new System.Drawing.Point(469, 63);
            this.volumeLabel.Name = "volumeLabel";
            this.volumeLabel.Size = new System.Drawing.Size(200, 13);
            this.volumeLabel.TabIndex = 6;
            this.volumeLabel.Text = "The volume used to play back the sound";
            // 
            // volumeTextBox
            // 
            this.volumeTextBox.Location = new System.Drawing.Point(703, 60);
            this.volumeTextBox.Name = "volumeTextBox";
            this.volumeTextBox.Size = new System.Drawing.Size(39, 20);
            this.volumeTextBox.TabIndex = 7;
            // 
            // searchTextBox
            // 
            this.searchTextBox.Location = new System.Drawing.Point(472, 28);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(197, 20);
            this.searchTextBox.TabIndex = 8;
            this.searchTextBox.Text = "Search sounds";
            // 
            // gainCheckBox
            // 
            this.gainCheckBox.AutoSize = true;
            this.gainCheckBox.Location = new System.Drawing.Point(472, 153);
            this.gainCheckBox.Name = "gainCheckBox";
            this.gainCheckBox.Size = new System.Drawing.Size(192, 17);
            this.gainCheckBox.TabIndex = 9;
            this.gainCheckBox.Text = "Apply a gain amplification on sound";
            this.gainCheckBox.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 450);
            this.Controls.Add(this.gainCheckBox);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.volumeTextBox);
            this.Controls.Add(this.volumeLabel);
            this.Controls.Add(this.volumeTrackBar);
            this.Controls.Add(this.editSoundButton);
            this.Controls.Add(this.removeSoundButton);
            this.Controls.Add(this.addSoundButton);
            this.Controls.Add(this.soundListView);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openSoundboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveSoundboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newSoundboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ListView soundListView;
        private System.Windows.Forms.Button addSoundButton;
        private System.Windows.Forms.Button removeSoundButton;
        private System.Windows.Forms.Button editSoundButton;
        private System.Windows.Forms.OpenFileDialog openSoundDialog;
        private System.Windows.Forms.OpenFileDialog openBoardDialog;
        private System.Windows.Forms.TrackBar volumeTrackBar;
        private System.Windows.Forms.Label volumeLabel;
        private System.Windows.Forms.TextBox volumeTextBox;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.CheckBox gainCheckBox;
    }
}

